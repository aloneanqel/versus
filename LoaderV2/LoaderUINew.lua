local library = {}
library.flags = {}
local RunService = game:GetService("RunService")
local Players = game:GetService("Players")
local LocalPlayer = Players.LocalPlayer
local Mouse = LocalPlayer:GetMouse()
local Hb = game:GetService("RunService").Heartbeat;


function library:CreateWindow(name)
    local Loader = Instance.new("ScreenGui")
    local MainFrame = Instance.new("Frame")
    local MainFrameCorner = Instance.new("UICorner")
    local TopLine = Instance.new("Frame")
    local CloseButton = Instance.new("TextButton")
    local Section = Instance.new("Frame")
    local SectionListLayout = Instance.new("UIListLayout")
    local SectionPadding = Instance.new("UIPadding")
    local Inputs = Instance.new("Frame")
    local InputListLayout = Instance.new("UIListLayout")
    local Buttons = Instance.new("Frame")
    local ButtonListLayout = Instance.new("UIListLayout")
    local LoaderTitle = Instance.new("TextLabel")

    Loader.Name = "Loader"
    Loader.Parent = game:GetService("CoreGui")
    
    LoaderTitle.Name = "LoaderTitle"
    LoaderTitle.Parent = MainFrame
    LoaderTitle.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    LoaderTitle.BackgroundTransparency = 1.000
    LoaderTitle.BorderSizePixel = 0
    LoaderTitle.Position = UDim2.new(0.0222222228, 0, 0, 0)
    LoaderTitle.Size = UDim2.new(0, 491, 0, 31)
    LoaderTitle.Font = Enum.Font.SourceSans
    LoaderTitle.Text = "Versus - discord.gg/8zzP4fXNFz" or name
    LoaderTitle.TextColor3 = Color3.fromRGB(255, 255, 255)
    LoaderTitle.TextSize = 22.000
    LoaderTitle.TextXAlignment = Enum.TextXAlignment.Left
    
    MainFrame.Name = "MainFrame"
    MainFrame.Parent = Loader
    MainFrame.BackgroundColor3 = Color3.fromRGB(28, 28, 28)
    MainFrame.BorderSizePixel = 0
    MainFrame.Position = UDim2.new(0.322644055, 0, 0.335802466, 0)
    MainFrame.Size = UDim2.new(0, 540, 0, 265)

    MainFrameCorner.CornerRadius = UDim.new(0.0199999996, 0)
    MainFrameCorner.Name = "MainFrameCorner"
    MainFrameCorner.Parent = MainFrame

    TopLine.Name = "TopLine"
    TopLine.Parent = MainFrame
    TopLine.BackgroundColor3 = Color3.fromRGB(113, 58, 162)
    TopLine.BorderSizePixel = 0
    TopLine.Position = UDim2.new(0, 0, 0.116981134, 0)
    TopLine.Size = UDim2.new(0, 539, 0, 5)

    CloseButton.Name = "CloseButton"
    CloseButton.Parent = MainFrame
    CloseButton.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    CloseButton.BackgroundTransparency = 1.000
    CloseButton.BorderSizePixel = 0
    CloseButton.Position = UDim2.new(0.948148131, 0, 0, 0)
    CloseButton.Size = UDim2.new(0, 27, 0, 31)
    CloseButton.Font = Enum.Font.Sarpanch
    CloseButton.Text = "X"
    CloseButton.TextColor3 = Color3.fromRGB(255, 255, 255)
    CloseButton.TextScaled = true
    CloseButton.TextSize = 14.000
    CloseButton.TextWrapped = true
    CloseButton.MouseButton1Click:Connect(function()
        Loader:Destroy()
    end)

    Section.Name = "Section"
    Section.Parent = MainFrame
    Section.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    Section.BackgroundTransparency = 1.000
    Section.BorderSizePixel = 0
    Section.Position = UDim2.new(0, 0, 0.135849059, 0)
    Section.Size = UDim2.new(0, 540, 0, 229)

    Inputs.Name = "Inputs"
    Inputs.Parent = Section
    Inputs.BackgroundColor3 = Color3.fromRGB(255, 0, 4)
    Inputs.BackgroundTransparency = 1.000
    Inputs.BorderSizePixel = 0
    Inputs.Position = UDim2.new(0, 0, 0.20960699, 0)
    Inputs.Size = UDim2.new(0, 540, 0, 97)

    InputListLayout.Name = "InputListLayout"
    InputListLayout.Parent = Inputs
    InputListLayout.HorizontalAlignment = Enum.HorizontalAlignment.Center
    InputListLayout.SortOrder = Enum.SortOrder.LayoutOrder
    InputListLayout.Padding = UDim.new(0.100000001, 2)

    SectionListLayout.Name = "SectionListLayout"
    SectionListLayout.Parent = Section
    SectionListLayout.HorizontalAlignment = Enum.HorizontalAlignment.Center
    SectionListLayout.SortOrder = Enum.SortOrder.LayoutOrder
    SectionListLayout.VerticalAlignment = Enum.VerticalAlignment.Center
    SectionListLayout.Padding = UDim.new(0, 50)

    SectionPadding.Name = "SectionPadding"
    SectionPadding.Parent = Section
    SectionPadding.PaddingBottom = UDim.new(0, 40)
    SectionPadding.PaddingTop = UDim.new(0.170000002, 1)

    ButtonListLayout.Name = "ButtonListLayout"
    ButtonListLayout.Parent = Buttons
    ButtonListLayout.FillDirection = Enum.FillDirection.Horizontal
    ButtonListLayout.HorizontalAlignment = Enum.HorizontalAlignment.Center
    ButtonListLayout.SortOrder = Enum.SortOrder.LayoutOrder
    ButtonListLayout.Padding = UDim.new(0.0299999993, 2)

    Buttons.Name = "Buttons"
    Buttons.Parent = Section
    Buttons.BackgroundColor3 = Color3.fromRGB(0, 200, 255)
    Buttons.BackgroundTransparency = 1.000
    Buttons.BorderSizePixel = 0
    Buttons.Position = UDim2.new(0, 0, 0.74840337, 0)
    Buttons.Size = UDim2.new(0, 539, 0, 44)

    local module = {}

    function module:Destroy()
        Loader:Destroy()
    end

    function module:CreateStatus(txt, text)
        local Status = Instance.new("TextLabel")

        Status.Name = txt
        Status.Parent = Inputs
        Status.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        Status.BackgroundTransparency = 1.000
        Status.BorderColor3 = Color3.fromRGB(27, 42, 53)
        Status.BorderSizePixel = 0
        Status.Position = UDim2.new(0.138888896, 0, 0, 0)
        Status.Size = UDim2.new(0, 295, 0, 29)
        Status.Font = Enum.Font.SourceSans
        Status.Text = text
        Status.TextColor3 = Color3.fromRGB(255, 255, 255)
        Status.TextSize = 22.000
        Status.TextWrapped = true
    end

    function module:CreateInput(txt)
        local KeyInput = Instance.new("TextBox")
        local KeyInputCorner = Instance.new("UICorner")
        local Warning = Instance.new("TextLabel")

        KeyInput.Name = "KeyBox"
        KeyInput.Parent = Inputs
        KeyInput.BackgroundColor3 = Color3.fromRGB(45, 45, 45)
        KeyInput.BorderSizePixel = 0
        KeyInput.Position = UDim2.new(0.200000003, 0, 0, 0)
        KeyInput.Size = UDim2.new(0, 262, 0, 50)
        KeyInput.Font = Enum.Font.SourceSans
        KeyInput.PlaceholderText = txt
        KeyInput.Text = ""
        KeyInput.TextColor3 = Color3.fromRGB(255, 255, 255)
        KeyInput.TextSize = 20.000
        
        KeyInputCorner.Name = "KeyInputCorner"
        KeyInputCorner.Parent = KeyInput

        Warning.Name = "Warning"
        Warning.Parent = Inputs
        Warning.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        Warning.BackgroundTransparency = 1.000
        Warning.BorderSizePixel = 0
        Warning.Position = UDim2.new(0.314814806, 0, 0, 0)
        Warning.Size = UDim2.new(0, 200, 0, 15)
        Warning.Font = Enum.Font.SourceSans
        Warning.Text = "Warning!  If your following key is 349934-D39f-VERSUS or WHY-STRIVE, it's invalid."
        Warning.TextColor3 = Color3.fromRGB(124, 124, 124)
        Warning.TextSize = 14.000
        Warning.TextYAlignment = Enum.TextYAlignment.Top
    end

    function module:CreateButton(txt, callback)
        local Button = Instance.new("TextButton")
        local ButtonCorner = Instance.new("UICorner")

        Button.Name = txt
        Button.Parent = Buttons
        Button.BackgroundColor3 = Color3.fromRGB(45, 45, 45)
        Button.BorderSizePixel = 0
        Button.Position = UDim2.new(0.0927643776, 0, 0, 0)
        Button.Size = UDim2.new(0, 139, 0, 44)
        Button.Font = Enum.Font.Ubuntu
        Button.Text = txt
        Button.TextColor3 = Color3.fromRGB(255, 255, 255)
        Button.TextSize = 18.000
        Button.MouseButton1Click:Connect(function()
            pcall(callback)
        end)

        ButtonCorner.Name = "ButtonCorner"
        ButtonCorner.Parent = Button
    end
    return module;
end
return library;