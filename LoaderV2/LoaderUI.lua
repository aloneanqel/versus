local libray = {}

local MainColor
local MainKeyColor

local Halloween = true

if Halloween == false then
    MainColor = Color3.fromRGB(19, 38, 209)
    MainKeyColor = Color3.fromRGB(37, 43, 99)
elseif Halloween == true then
    MainColor = Color3.fromRGB(255, 123, 0)
    MainKeyColor = Color3.fromRGB(197, 123, 27)
end

function libray:createWindow(title)
    local Loader = Instance.new("ScreenGui")
    local MainFrame = Instance.new("Frame")
    local Bar = Instance.new("Frame")
    local Title = Instance.new("TextLabel")
    local Container = Instance.new("Frame")
	local Close = Instance.new("TextButton")
    local Discord = Instance.new("TextButton")

    local UIS = game:GetService("UserInputService")

	function dragify(Frame)
		dragToggle = nil
		local dragSpeed = 0
		dragInput = nil
		dragStart = nil
		local dragPos = nil
		function updateInput(input)
			local Delta = input.Position - dragStart
			local Position = UDim2.new(startPos.X.Scale, startPos.X.Offset + Delta.X, startPos.Y.Scale, startPos.Y.Offset + Delta.Y)
			game:GetService("TweenService"):Create(Frame, TweenInfo.new(0.25), {Position = Position}):Play()
		end
		Frame.InputBegan:Connect(function(input)
			if (input.UserInputType == Enum.UserInputType.MouseButton1 or input.UserInputType == Enum.UserInputType.Touch) and UIS:GetFocusedTextBox() == nil then
				dragToggle = true
				dragStart = input.Position
				startPos = Frame.Position
				input.Changed:Connect(function()
					if input.UserInputState == Enum.UserInputState.End then
						dragToggle = false
					end
				end)
			end
		end)
		Frame.InputChanged:Connect(function(input)
			if input.UserInputType == Enum.UserInputType.MouseMovement or input.UserInputType == Enum.UserInputType.Touch then
				dragInput = input
			end
		end)
		game:GetService("UserInputService").InputChanged:Connect(function(input)
			if input == dragInput and dragToggle then
				updateInput(input)
			end
		end)
	end
	
	dragify(MainFrame)

    Loader.Name = "Loader"
    Loader.Parent = game:GetService("CoreGui")

    MainFrame.Name = "MainFrame"
    MainFrame.Parent = Loader
    MainFrame.BackgroundColor3 = Color3.fromRGB(25, 25, 25)
    MainFrame.BorderSizePixel = 0
    MainFrame.Position = UDim2.new(0.389265925, 0, 0.400000006, 0)
    MainFrame.Size = UDim2.new(0, 358, 0, 250)

    Bar.Name = "Bar"
    Bar.Parent = MainFrame
    Bar.BackgroundColor3 = MainColor
    Bar.BorderSizePixel = 0
    Bar.Position = UDim2.new(0, 0, 0.130801693, 0)
    Bar.Size = UDim2.new(0, 358, 0, 2)


    Title.Name = "Title"
    Title.Parent = MainFrame
    Title.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    Title.BackgroundTransparency = 1.000
    Title.BorderSizePixel = 0
    Title.Size = UDim2.new(0, 358, 0, 31)
    Title.Font = Enum.Font.SourceSans
    Title.Text = title
    Title.TextColor3 = Color3.fromRGB(193, 193, 193)
    Title.TextSize = 26.000

    Container.Name = "Container"
    Container.Parent = MainFrame
    Container.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    Container.BackgroundTransparency = 1.000
    Container.BorderSizePixel = 0
    Container.Position = UDim2.new(0, 0, 0.633692741, 0)
    Container.Size = UDim2.new(0, 358, 0, 86)

	Close.Name = "Close"
	Close.Parent = MainFrame
	Close.BackgroundColor3 = Color3.fromRGB(19, 38, 209)
    Close.BackgroundTransparency = 1
	Close.BorderSizePixel = 0
	Close.Position = UDim2.new(0.90, 0,0, 0)
	Close.Size = UDim2.new(0, 41, 0, 32)
	Close.ZIndex = 3
	Close.Font = Enum.Font.SourceSans
	Close.Text = "X"
	Close.TextColor3 = Color3.fromRGB(255, 255, 255)
	Close.TextScaled = true
	Close.TextSize = 20.000
	Close.TextWrapped = true

    Discord.Name = "Discord"
    Discord.Parent = MainFrame
    Discord.BackgroundColor3 = Color3.fromRGB(0, 118, 173)
    Discord.BackgroundTransparency = 1.000
    Discord.BorderSizePixel = 0
    -- Discord.Position = UDim2.new(0, 0, 0.997559726, 0)
    Discord.Position = UDim2.new(0, 0, 0.58, 0)
    Discord.Size = UDim2.new(0, 358, 0, 32)
    Discord.ZIndex = 3
    Discord.Font = Enum.Font.SourceSans
    Discord.Text = "https://discord.gg/8zzP4fXNFz"
    Discord.TextColor3 = Color3.fromRGB(255, 255, 255)
    Discord.TextSize = 26.000
    Discord.TextWrapped = true

    Discord.MouseButton1Click:Connect(function()
        setclipboard("https://discord.gg/8zzP4fXNFz")
        Discord.Text = "Copied!"
        wait(1)
        Discord.Text = "https://discord.gg/8zzP4fXNFz"
    end)

	Close.MouseButton1Click:Connect(function()
		Loader:Destroy()
	end)

    local window = {}

    function window:Destroy()
        Loader:Destroy()
    end

    function window:CreateLabel(text)
        local Label = Instance.new("TextLabel")

        Label.Name = "Label"
        Label.Parent = MainFrame
        Label.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        Label.BackgroundTransparency = 1.000
        Label.BorderSizePixel = 0
        Label.Position = UDim2.new(0, 0, 0.277777791, 0)
        Label.Size = UDim2.new(0, 358, 0, 31)
        Label.Font = Enum.Font.SourceSans
        Label.Text = text
        Label.TextColor3 = Color3.fromRGB(193, 193, 193)
        Label.TextSize = 26.000
    end

    function window:CreateButton(text,cb)
        local Button = Instance.new("TextButton")
        local Button_Roundify_10px = Instance.new("ImageLabel")
        local UIGridLayout = Instance.new("UIGridLayout")

        Button_Roundify_10px.Name = "Button_Roundify_10px"
        Button_Roundify_10px.Parent = Button
        Button_Roundify_10px.Active = true
        Button_Roundify_10px.AnchorPoint = Vector2.new(0.5, 0.5)
        Button_Roundify_10px.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        Button_Roundify_10px.BackgroundTransparency = 1.000
        Button_Roundify_10px.Position = UDim2.new(0.5, 0, 0.5, 0)
        Button_Roundify_10px.Selectable = true
        Button_Roundify_10px.Size = UDim2.new(1, 0, 1, 0)
        Button_Roundify_10px.Image = "rbxassetid://3570695787"
        Button_Roundify_10px.ImageColor3 = MainColor
        Button_Roundify_10px.ScaleType = Enum.ScaleType.Slice
        Button_Roundify_10px.SliceCenter = Rect.new(100, 100, 100, 100)
        Button_Roundify_10px.SliceScale = 0.100
        
        UIGridLayout.Parent = Container
        UIGridLayout.HorizontalAlignment = Enum.HorizontalAlignment.Center
        UIGridLayout.SortOrder = Enum.SortOrder.LayoutOrder
        UIGridLayout.VerticalAlignment = Enum.VerticalAlignment.Center
        UIGridLayout.CellPadding = UDim2.new(0, 10, 0, 3)
        UIGridLayout.CellSize = UDim2.new(0, 150, 0, 40)
        
        Button.Name = "Button"
        Button.Parent = Container
        Button.BackgroundColor3 = Color3.fromRGB(19, 38, 209)
        Button.BackgroundTransparency = 1.000
        Button.BorderSizePixel = 0
        Button.Position = UDim2.new(0, 0, 0, 0)
        Button.Size = UDim2.new(0, 116, 0, 32)
        Button.ZIndex = 3
        Button.Font = Enum.Font.SourceSans
        Button.Text = text
        Button.TextColor3 = Color3.fromRGB(255, 255, 255)
        Button.TextSize = 20.000
        Button.MouseButton1Click:Connect(function()
            pcall(cb)
        end)
    end

    function window:CreateKeyBox(placeholder)
        local KeyBox = Instance.new("TextBox")
        local KeyBoxCorner = Instance.new("UICorner")

        KeyBox.Name = "KeyBox"
        KeyBox.Parent = MainFrame
        KeyBox.BackgroundColor3 = MainKeyColor
        KeyBox.Position = UDim2.new(0.19273743, 0, 0.430379748, 0)
        KeyBox.Size = UDim2.new(0, 220, 0, 32)
        KeyBox.Font = Enum.Font.SourceSans
        KeyBox.PlaceholderText = placeholder
        KeyBox.Text = ""
        KeyBox.TextColor3 = Color3.fromRGB(255, 255, 255)
        KeyBox.TextSize = 26.000

        KeyBoxCorner.CornerRadius = UDim.new(0, 8)
        KeyBoxCorner.Name = "KeyBoxCorner"
        KeyBoxCorner.Parent = KeyBox
    end

    return window
end
return libray