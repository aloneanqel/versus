local library = loadstring(game:HttpGet("https://gitlab.com/Ner0ox/versus/-/raw/main/ui/UIV2.lua", true))()
local ui = library:CreateWindow("Versus - Universal")

local plr = game:GetService("Players").LocalPlayer
local VU = game:GetService("VirtualUser")
local WS = game:GetService("Workspace")
local tpService = game:GetService("TeleportService")

local PlrTab = ui:new("Player")
local Misc = ui:new("Misc")


plr.Idled:connect(function()
    VU:Button2Down(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
    wait(1)
    VU:Button2Up(Vector2.new(0, 0), WS.CurrentCamera.CFrame)
end)


-- PLAYER

PlrTab:CreateSlider("Walkspeed", {min = 16, max = 200, default = 16}, function(state)
    plr.Character.Humanoid.WalkSpeed = state;
end)

PlrTab:CreateSlider("JumpPower", {min = 50, max = 200, default = 50}, function(state)
    plr.Character.Humanoid.JumpPower = state;
end)

PlrTab:CreateSlider("Hip Height", {min = 0, max = 200, default = 0}, function(state)
    plr.Character.Humanoid.HipHeight = state;
end)

PlrTab:CreateButton("Respawn player", function()
    plr.Character:BreakJoints()
end)

PlrTab:CreateToggle("Loop respawn player", true, function()
    plr.Character:BreakJoints()    
end)


-- MISC

Misc:CreateButton("Copy Discord invite", function()
    setclipboard("https://discord.gg/VdwR6MRqAx")
end)

Misc:CreateButton("Rejoin Server", function()
    tpService:Teleport(game.PlaceId)
end)

Misc:CreateButton("Uninject", function()
    game:GetService("CoreGui")["UI V2"]:Destroy()
end)